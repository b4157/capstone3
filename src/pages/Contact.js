import React, { useState, useEffect} from 'react';
import  {Container, Col, Row, Form, Button} from "react-bootstrap";
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import emailjs from 'emailjs-com';
import { init } from 'emailjs-com';
init('user_id');

export default function Contact(){

    const navigate = useNavigate();
    const routeChange = () =>{ 
        navigate('/')
     }
    const [fullName, setName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNum, setMobileNum] = useState('');
    const [message, setMessage] = useState('');
    const [isActive, setIsActive] = useState(true);
  
   useEffect(()=>{
        if(fullName !=='' && email !== '' && mobileNum !=='' && message!==''){
            setIsActive(true);
        } else{
            setIsActive(false);
        }
    }, [fullName, email, mobileNum, message])    


    function submit(e) {
        e.preventDefault();
    
            const serviceId = 'service_y96qp6a';
            const templateId = 'template_7t8an6u';
            const userId = 'HGQnFZsC93TPHkWiz';
            const templateParams = {
                fullName,
                email,
                mobileNum,
                message
            }
    
            emailjs.send(serviceId, templateId, templateParams, userId)
              .then(response => 
                {response ? 
                    Swal.fire({
                      title: 'Good job!',
                      text: `Message Sent. Thank you for your message. We will be in touch in no time`,
                      icon:'success'
                    })
                    :
                    setName('');
                    setEmail('');
                    setMobileNum('');
                    setMessage('');
                }

            )
                .then(error => console.log(error));   
    }

    
    return (
    
        <Container>
            <Row>
                <Col className = "p-3 m-3">
                    <Form onSubmit={(e) => submit(e) }>
                        <Form.Group>
                            <h1 className = "text-center mb-2 mt-2 loginText"  id="contactform">Contact Form</h1>
                            <Form.Label>Name</Form.Label>
                            <Form.Control 
                                type="text"
                                placeholder = "(Required) ex.: Juan Dela Cruz"
                                required    
                                value={fullName}
                                onChange={e => setName(e.target.value)}
                            />           
                        </Form.Group>

                         <Form.Group className = "mt-4">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control 
                                type="email"
                                placeholder = "(Required) ex.: juan@email.com"
                                required    
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                            />           
                        </Form.Group>

                        <Form.Group className = "mt-4">
                            <Form.Label>Mobile Number</Form.Label>
                            <Form.Control 
                                type="number"
                                placeholder = "(Required) ex.: 0917 113 4532"
                                required    
                                value={mobileNum}
                                onChange={e => setMobileNum(e.target.value)}
                            />           
                        </Form.Group> 

                        <Form.Group className = "mt-4">
                            <Form.Label>Message</Form.Label>
                            <Form.Control 
                                as="textarea"
                                placeholder = "(Required) Write your message here"
                                required
                                rows={10}    
                                value={message}
                                onChange={e => setMessage(e.target.value)}
                            />           
                        </Form.Group>

                        <Form.Group className="mt-3 p-3">

                            {isActive ?
                                <Row>
                                    <Button className="loginBut mt-3 p-1" type="submit"> Send Message </Button> 
                                    <Button className="mt-3 p-1" variant="secondary" onClick={routeChange}> Cancel </Button>  
                                    <Button className="loginBut mt-3 p-1" onClick={routeChange}> Reset Fields </Button>  
                                    <Button className="mt-3 p-1" variant="secondary" onClick={routeChange}> Cancel </Button>   
                                </Row>
                                :
                                <Row>
                                    <Button className="loginBut mt-3 p-1" type="submit" disabled> Send Message </Button> 
                                     <Button className="loginBut mt-3 p-1" onClick={routeChange}> Reset Fields </Button>   
                                     <Button className="mt-3 p-1" variant="secondary" onClick={routeChange}> Cancel </Button>   
                                </Row>
                            }
                                
                        </Form.Group>       
                    </Form>
                </Col>
                <Col className = "p-3 m-3">
                     <h1 className = "text-center mb-2 mt-2 loginText"  id="contactform">Contact Details</h1>
                   <Row>
                       <Col xs={12} md={6}>
                            <img className = "d-flex flex-column p-1" src={require("../images/mobile.png")} width="10%" height="50%" alt='mobile icon'/>(+63) 919-009-3050
                        </Col>
                       <Col xs={12} md={6}>
                            <span><img className = "d-flex flex-column p-1" src={require("../images/email.png")} width="10%" height="50%" alt='email icon'/></span> 
                            <span>lagudagrocerystore@gmail.com</span>
                        </Col>
                   </Row> 
                   <Row>
                       <Col xs={12} md={6}>
                            <span><img className = "d-flex flex-column p-1" src={require("../images/telephone.png")} width="10%" height="50%" alt='telephone icon'/></span> 
                            <span>(056) 211-2910</span>
                        </Col>
                       <Col xs={12} md={6}>
                            <span><img className = "d-flex flex-column p-1" src={require("../images/home.png")} width="10%" height="50%" alt='home icon'/></span> 
                            <span>Purok 1 Tabon-Tabon, Irosin, Sorsogon 4707</span>
                       </Col>
                   </Row>
                   <Row className="mt-4">
                         <h5><strong>Visit Laguda Grocery Store!</strong></h5>
                         <iframe title= "Laguda Grocery Store map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15568.694571615035!2d124.042495!3d12.702087!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x60bd7fba15bac242!2sLaguda%20Grocery%20Store!5e0!3m2!1sen!2sph!4v1653217695461!5m2!1sen!2sph" width="400" height="300" style={{border:0}} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
                    </Row>
                   <Row className="mt-2">
                        <h5><strong>Visit Dakjuk!</strong></h5>
                        <iframe title= "Dakjuk map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3892.173820655157!2d124.04029931464206!3d12.702075391033116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x33a0cf1a57824ed9%3A0x5d6382c61c75bbf5!2sDakjuk!5e0!3m2!1sen!2sph!4v1653218388197!5m2!1sen!2sph" width="400" height="300" style={{border:0}} allowFullScreen="" loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
                   </Row> 
                </Col>
            </Row>

        </Container>


    );
};

