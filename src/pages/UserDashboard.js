import React, {useState, useEffect} from 'react';


import NormalUserViewAccount from '../components/NormalUserViewAccount';
// import UserContext from '../UserContext';
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'

export default function UserDashboard(){

	// const { user } = useContext (UserContext)
	// const [allProductsMenu, setProductsMenu] = useState([]);
	const [allOrders, setAllOrders] = useState([]);

	const fetchDataOrder = () => {
		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/orders/all-auth-orders',{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {
			setAllOrders(data)
		})

	}


	useEffect(()=> {
		fetchDataOrder()
	}, [])

	return (
		<>		
				<div className="text-center my-4">
					<h1 className="welcome">Dashboard</h1>
					<Tabs defaultActiveKey="orders" id="uncontrolled-tab-example" className="mb-3">
					  <Tab eventKey="orders" title="Order History">
					   		<NormalUserViewAccount ordersData={allOrders} fetchData={fetchDataOrder}/>
					  </Tab>
					  <Tab eventKey="ship" title="Shipping Address">
					   		<p>To be developed pa po</p>
					  </Tab>
					  <Tab eventKey="account" title="Manage Account">		   	 
					   		<p>To be developed pa po</p>
					  </Tab>
					</Tabs>
				</div>			

		</>
	)

}