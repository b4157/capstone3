import React, {useState, useEffect, useContext} from 'react';

import AdminViewOrderHistory from "../components/AdminViewOrderHistory"
import ManageUser from "../components/ManageUser"
// import	coursesData from "../mockData/coursesData";
// import CourseCard from '../components/ProductCards';
import UserView from '../components/UserView';
import AdminView from '../components/AdminView';
import UserContext from '../UserContext';
import Tab from 'react-bootstrap/Tab'
import Tabs from 'react-bootstrap/Tabs'

export default function Menu(){

	const { user } = useContext (UserContext)
	
	const [allProductsMenu, setProductsMenu] = useState([]);
	const [allOrders, setAllOrders] = useState([]);
	const [allUsers, setAllUsers] = useState([]);

	const fetchData = () => {
		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/products/product-list-dakjuk',{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
			})
		.then(res=> res.json())
		.then (data => {
			setProductsMenu(data)
		})
	}


	const fetchDataOrder = () => {
		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/orders/all-orders',{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {
			setAllOrders(data)
		})

	}

	const fetchDataUsers = () => {
		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/users/all',{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
		})
		.then(res=> res.json())
		.then (data => {
			setAllUsers(data)

		console.log(data)
		})

	}

	useEffect(()=> {
		fetchData()
		fetchDataOrder()
		fetchDataUsers()
	}, [])

	return (
		<>
			{ (user.isAdmin !== true)?	

				<UserView productsData={allProductsMenu}/>
				:
					<div className="text-center my-4">
						<h1 className="welcome">Admin Dashboard</h1>
						<Tabs defaultActiveKey="products" id="uncontrolled-tab-example" className="mb-3">
						  <Tab eventKey="products" title="Products">
						   		<AdminView productsData={allProductsMenu} fetchData={fetchData}/>
						  </Tab>
						  <Tab eventKey="orders" title="Orders">
						   		<p><AdminViewOrderHistory ordersData={allOrders} fetchData={fetchDataOrder}/></p>
						  </Tab>
						  <Tab eventKey="users" title="Users">		   	 
						   		<p><ManageUser usersData={allUsers} fetchData={fetchDataUsers}/></p>
						  </Tab>
						</Tabs>
					</div>			
			}

		


		</>
	)

}