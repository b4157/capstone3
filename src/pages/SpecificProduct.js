import React, { useState, useEffect, useContext } from 'react';
import {Row, Container, Card, Button, Col } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import Image from 'react-bootstrap/Image'

export default function SpecificProduct() {


	const navigate = useNavigate();

	//useParams() contains any values we are tryilng to pass in the URL stored
	//useParams is how we receive the courseId passed via the URL
	const { productId } = useParams();

	const [subTotal, setSubTotal] = useState(0);
	const [prodCount, setProdCount] = useState(0);

	useEffect(() => {

		fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/products/${productId}`, {
			method: 'GET'
		})
		.then(res => res.json())
		.then(data => {
			setName(data.productName)
			setDescription(data.description)
			setPrice(data.price)
			setPhoto(data.photo)
			
		})

	} ,[prodCount, subTotal, productId])

	const { user } = useContext(UserContext);

	const [productName, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')	
	const [photo, setPhoto] = useState('')
	const [minusBut, setMinusBut] = useState(true)

	const addCart = (productId) => {

		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/orders/add-to-cart', {
			
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
				'Accept': 'application/json'
			},

			body: JSON.stringify({
				totalAmount: subTotal,
				productOrders:[
					{
						_id: productId,
						productName: productName,
						prodQty: prodCount
					}
				]
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Product successfully added to cart',
					icon: 'success'
				})

				navigate('/menu')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		}) 
	}

	

		// let photoId = "'https://drive.google.com/file/d/1G0_-1SBmkGsXLqUAAC7q1miuy83fK5Fe/preview'"
		// let imgUrl = 'https://drive.google.com/thumbnail?id=' + photoId +'"'

	return (


		<Container className='m-3'>
			<Row className="xs={12} md={6}">
				<Card style={{ width: '40rem' }}>
					<Card.Header>
						<h4>{productName}</h4>
					</Card.Header>

					<Card.Body>
						<Image style={{height:'auto',width:'30%'}}  src={`../images/products/${photo}`}></Image>
						{/*<Card.Img variant="top" src="holder.js/100px180" />*/}
						<Card.Text className="m-3">{description}</Card.Text>
						<strong><h6 className="m-3">Price: &#8369; {price}</h6></strong>
						{/*<iframe src={require(photoId)} width="100" height="50" allow="autoplay"></iframe>*/}
						<Row lg={4}>	
							<Col>
								<Button 
								onClick={() => { setProdCount(prodCount - 1); 
									setSubTotal(subTotal - price);
									if (prodCount<2) {
										return setMinusBut(true)
									} else{
										return setMinusBut(false)
									}
								}} 
								
								disabled={minusBut}>-</Button>
							</Col>
							<Col>
								<Card.Text className="text-muted"> { prodCount }</Card.Text>
							</Col>
							<Col>
								<Button onClick={() => { setProdCount(prodCount + 1); 
									setSubTotal(subTotal + price)
									if (prodCount <0) {
										return setMinusBut(true)
									} else{
										return setMinusBut(false)
									}
								}}

								>+</Button>
							</Col>
							<Col>
								<Card.Text> Sub-Total:{ subTotal }</Card.Text>
							</Col>
						</Row>

					</Card.Body>

					<Card.Footer>

						{user.accessToken !== null ?
							<div className="d-grip gap-3">
								<Button variant="primary" onClick={() => addCart(productId)}>Add to Cart</Button>
							</div>
							

							:

							<Link className="btn btn-warning d-grip gap-3" to="/login">Login to order</Link>
						}
						
					</Card.Footer>
				</Card>
			</Row>
		</Container>

		)
}
