import React, {useState, useEffect, useContext} from 'react';
import	{Container, Row, Col, Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import {Link} from 'react-router-dom';
 import UserContext from '../UserContext';

import {Navigate, useNavigate} from 'react-router-dom';

export default function Register(){
	
	const {user} = useContext(UserContext);
	const navigate = useNavigate();

	// store values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(true);
	const [lastName, setLastName] = useState('');
	const [firstName, setFirstName] = useState('');
	const [mobileNum, setMobileNum] = useState('');
	
	const routeChange = () =>{ 
	  	navigate('/')
	 }


	 function checkEmail(e){

	 	e.preventDefault();

	 	console.log(email)

	 	fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/users/${email}`,{
	 	// fetch(`http://localhost:4000/users/${email}`, {
			method: 'GET'

			// headers: {
			// 		'Content-Type': 'application/json',
			// 		// Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
			// 		 'Accept': 'application/json'
			// 	}
		})
		.then(response=> response.json())
		.then (data => {
	
			console.log(data)

			if (data !== null){
				Swal.fire({
				  title: 'Oppss!',
				  text: `Something went wrong. Check your credentials.!`,
				  icon:'error'
				})
				setEmail('');
			}
		})	
	 }

	useEffect(()=>{
		if(email !=='' && password1 !== '' && password2 !=='' && lastName!=='' && firstName !=='' && mobileNum!==''){
			
			if (password1 !== password2) {
				Swal.fire({
				  title: 'Password Mismatch',
				  text: 'Your password did not match. Try again!',
				  icon:'error'
				})		
			}
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password1, password2, lastName, firstName, mobileNum])

	
	function registerUser(e){
		e.preventDefault();


		// fetch('http://localhost:4000/users/register', {
		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/users/register', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				firstName: firstName,
			    lastName: lastName,
			    email: email,
			    password: password1,
			    mobileNum: mobileNum
			})
		
		})	
		.then(response => response.json())
		.then(data => {		
			Swal.fire({
			  title: 'Good job!',
			  text: 'Registration successful! Login Now!',
			  icon:'success'
			})
			setEmail('');
			setPassword1('');
			setPassword2('');
			setMobileNum('');
			setFirstName('');
			setLastName ('');
			navigate('/login')
		})	

		
	}

	return (
		(user.accessToken !== null) ?
			<Navigate to="/" />
		:
			<Container>
				<Form className = "p-1" onSubmit={(e) => registerUser(e) }>
						<h1 className = "text-center mb-2 mt-4 loginText">Register</h1>
						<Row className="justify-content-md-center">	
							<Col s={12} md={6}>
								<Form.Group>
									<Form.Label>Email Address</Form.Label>
									<Form.Control 
										type="email"
										placeholder = "ex.: juan@email.com"
										required
										value={email}
										onChange={e => setEmail(e.target.value)}
										onBlur = {e => checkEmail(e)}
									/>
									<Form.Text className = "text-muted">
										We will never share your email with anyone else.
									</Form.Text>
								</Form.Group>
								<Form.Group>
									<Form.Label>Password</Form.Label>
									<Form.Control 
										type="password"
										placeholder = "Enter password"
										required
										value={password1}
										onChange={e => setPassword1(e.target.value)}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label>Confirm Password</Form.Label>
									<Form.Control 
										type="password"
										placeholder = "Verify password"
										required
										value={password2}
										onChange={e => setPassword2(e.target.value)}
									/>
								</Form.Group>
						
								<Form.Group>
									<Form.Label>First Name</Form.Label>
									<Form.Control 
										placeholder = "ex: Juan"
										required
										value={firstName}
										onChange={e => setFirstName(e.target.value)}
									/>
								</Form.Group>
								<Form.Group>
									<Form.Label className="mt-4">Last Name</Form.Label>
									<Form.Control 
										placeholder = "ex: Dela Cruz"
										required
										value={lastName}
										onChange={e => setLastName(e.target.value)}
									/>
								</Form.Group>

								<Form.Group>
									<Form.Label className="mt-4">Mobile Number</Form.Label>
									<Form.Control 
										placeholder = "ex.: 0917 1123 3289"
										required
										value={mobileNum}
										onChange={e => setMobileNum(e.target.value)}
									/>
								</Form.Group>
							</Col>
						</Row>	
						{isActive ?
							<Row xs={12} md={6} className="m-3 p-1 justify-content-md-center">
								<Button className="loginBut m-2 p-1" type="submit" > Register </Button>
								<Button className="loginBut m-2 p-1" onClick={routeChange}> Reset Fields </Button>  
								<Button className="m-2 p-1" variant="secondary" onClick={routeChange} > Cancel </Button>
							</Row>
							:
							<Row xs={12} md={6} className="	m-3 p-1 justify-content-md-center" >
								<Button className="loginBut m-1" type="submit" disabled> Register </Button>
								<Button className="loginBut m-1" onClick={routeChange}> Reset Fields </Button>  
								<Button className="m-1" variant="secondary" onClick={routeChange} > Cancel </Button>
							</Row>
						}
						
						<p className="text-center"> Already resgistered? Login <Link to={'/login'}>here</Link></p>
					</Form>		

			</Container>	
		)
}