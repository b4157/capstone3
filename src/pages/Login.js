import React, {useState, useEffect, useContext} from 'react';
import	{Col, Row, Form, Button} from "react-bootstrap";
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import {Navigate, useNavigate} from 'react-router-dom';
import {Link} from 'react-router-dom';

export default function Login(){
	const {user, setUser} = useContext(UserContext);
	const navigate = useNavigate();
	// store values of input fields
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');

	const [isActive, setIsActive] = useState(true);

	useEffect(()=>{
		if(email !=='' && password1 !== ''){
			setIsActive(true);
		} else{
			setIsActive(false);
		}
	}, [email, password1])

	
	 const routeChange = () =>{ 
	  	navigate('/')
	 }

	function loginUser(e) {
		e.preventDefault();

		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/users/login', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(data => {
			console.log(data)

			if (data.accessToken !== undefined){
				localStorage.setItem('accessToken', data.accessToken);
				localStorage.setItem('email', data.email);

					
				setUser({
					accessToken: data.accessToken})

				setEmail('');
				setPassword1('');

				Swal.fire({
				  title: 'Good job!',
				  text: `Login successful!`,
				  icon:'success'
				})

				if(data.isAdmin === true){
						localStorage.setItem('email', data.email);
						localStorage.setItem('isAdmin', data.isAdmin);
						setUser({
							email: data.email,
							isAdmin: data.isAdmin

						})

						navigate('/Menu')
					} else{
						localStorage.setItem('email', data.email);
						localStorage.setItem('isAdmin', data.isAdmin);
						setUser({
							email: data.email,
							isAdmin: data.isAdmin

						})
						// to be changed in the future
						navigate('/Menu')
					}



				// fetch('http://localhost:4000/users/single-user', {
				// 	headers: {
				// 		Authorization: `Bearer ${data.accessToken}`,
				// 	}
				// })
				// .then(res=>res.json())
				// .then(result=>{
				// 	console.log(result.isAdmin)
					
				// })

			}else{

				Swal.fire({
				  title: 'Oppss!',
				  text: `Something went wrong. Check your credentials.!`,
				  icon:'error'
				})
			}
		})	

		// 	e.preventDefault();
		
	}
	const navToDashboard = () =>{
		<Navigate to='/' />
	}

	return (
		(user.accessToken !== null) ?
			<Navigate to="/" />
		:
			<Row className="justify-content-md-center">
				<Col s={12} md={6}>
					<Form className = "p-1 m-5" onSubmit={(e) => loginUser(e) } onClick = {()=>navToDashboard()}>
						<Form.Group >
							<h1 className = "text-center mb-2 loginText">Login</h1>
							<Form.Label>Email Address</Form.Label>
							<Form.Control 
								type="email"
								placeholder = "ex.: juan@email.com"
								required	
								value={email}
								onChange={e => setEmail(e.target.value)}
							/>
						
						</Form.Group>

						<Form.Group className = "mt-3">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password"
								placeholder = "Enter password"
								required
								value={password1}
								onChange={e => setPassword1(e.target.value)}
							/>
						
						</Form.Group>
						
						{isActive ?
								<Row className="mt-3 p-3">
									<Button className="loginBut" type="submit" > Login </Button>
									<Button className="mt-1" variant="secondary" onClick={()=>routeChange()}> Cancel </Button>
								</Row>
								:
								<Row className="mt-3 p-3" >
									<Button className="loginBut" type="submit" disabled> Login </Button>
									<Button className="mt-1" variant="secondary" onClick={()=>routeChange()}> Cancel </Button>
								</Row>
							}
						<p className="text-center"> Not yet a user? Register <Link to={'/register'}>here</Link></p>
						
					</Form>

					
				</Col>	

				<Row>
					
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
					<br/>
				</Row>		
			</Row>
			
			
	)
}