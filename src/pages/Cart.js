import React, {useState, useEffect} from 'react';
// import {Link} from 'react-router-dom';
// import {Navigate} from 'react-router-dom';
// import Alert from 'react-bootstrap/Alert'
import UserViewCart from '../components/UserViewCart';
// import UserContext from '../UserContext';
// import { useNavigate } from 'react-router-dom';

export default function Menu(){
	// const { user } = useContext (UserContext)
	// const [allOrders, setAllOrders] = useState([]);
	const [allProductsMenu, setProductsMenu] = useState([]);
	// const [isCartEmpty, setIsCartEmpty] = useState([false]);


	const fetchData = () => {
		
		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/orders/cart',{
			method: 'GET',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'

				}
			// 	,
			// body: JSON.stringify({
			// 	id: email,
			// 	})
			})
		.then(res=> res.json())
		.then (data => {
			console.log(data)
			setProductsMenu(data)
			// if (allProductsMenu.length ===0) {
			// 	setIsCartEmpty(true)
			// } else {
			// 	setIsCartEmpty(false)
			// }
		})

	}


	// const fetchDataOrder = () => {
	// 	fetch('http://localhost:4000/orders/cart-products',{
	// 		method: 'GET',
	// 			headers: {
	// 				'Content-Type': 'application/json',
	// 				Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
	// 				 'Accept': 'application/json'
	// 			}
	// 	})
	// 	.then(res=> res.json())
	// 	.then (data => {
	// 		console.log(data)
	// 		setAllOrders(data)
	// 	})

	// }

	useEffect( () 	=> {			
		// fetchDataOrder()
		fetchData() 

	}, [])



	return (
		// <UserViewCart ordersData={allOrders} fetchData={fetchDataOrder} productsData={allProductsMenu} fetchDataProduct={fetchData}/>
		//  (user.accessToken === null) ?
		// 	<Navigate to="/" />
		// :
		//  (isCartEmpty === true) ?

		//  	<Alert variant="danger">
		//  		Your cart is empty. <Link to={'/menu'}>Shop now!</Link>
		//  	</Alert>
			

		// 	:

			<UserViewCart productsData={allProductsMenu} fetchDataProduct={fetchData}/>
	)

}