import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product, isActive, fetchData}) {

	const archiveToggle = (productId) => {
			
			fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/products/${productId}/set-active-stat`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					'Accept': 'application/json'
				}
			})
			.then(data => {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Course successfully disabled'
					})
					fetchData()
			})

	}

	const reactivate = (productId) => {
			fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/products/${productId}/set-active-stat`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
			})
			.then(data => {

					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Product successfully reactivated'
					})
					fetchData()
			})
		}	
	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => reactivate(product)}>Unarchive</Button>
			}
		</>

		)
}