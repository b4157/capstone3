import React from 'react';
import	{Row, Col, Card, Button} from "react-bootstrap";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function ProductCards({productProp}){

	const { _id, productName, description, price} = productProp;
	
	return (
		<Row className="pt-3">
			<Col xs={12} md={6}>
				<Card className="courseCard p-3">
					<Card.Body>
						<Card.Title><h3>{ productName }</h3></Card.Title>
						<br/>			
						<Card.Subtitle><h5>Description</h5></Card.Subtitle>
						<Card.Text>{ description }</Card.Text>
						<Card.Subtitle><h5>Price:</h5></Card.Subtitle>
						<Card.Text>&#8369; { price }</Card.Text>
						<Link to={`/products/${_id}`}><Button className="btn">Read More details</Button> </Link>
					</Card.Body>

				</Card>
			</Col>
		</Row>
	)

}

ProductCards.propTypes = {
	productProp: PropTypes.shape({
		productName: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
		// photo: PropTypes.string.isRequired
	})
}


