import React, {useState, useEffect} from 'react';
import	ProductCards from "./ProductCards";

export default function UserView({productsData}){
	
	const [products,setProducts] = useState([])
	
	useEffect(()=> {
		
		const productsArr = productsData.map(product => {

			if (product.isActive === true){
				return (
					<ProductCards productProp = {product} key = {product._id}/>
					)
			} else{
				return null;
			}
		})
		setProducts(productsArr)
	}, [productsData])

	return(
		<>
			{products}
		</>
	)
}