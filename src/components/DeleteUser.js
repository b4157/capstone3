import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteUser({user, fetchData}) {

	const deleteUser = (userId) => {
			
			fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/users/${userId}/delete-user-account`, {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					
				}
			})
			.then(data => {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'User successfully deleted'
					})
					fetchData()
			})

	}

	return(

		<>
			<Button className= "btnDanger" variant="danger" size="sm" onClick={() => deleteUser(user)}>Delete</Button>

				
		</>

		)
}