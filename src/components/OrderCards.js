import React from 'react';
import	{Row, Col, Card} from "react-bootstrap";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

export default function OrderCards({orderProp}){

	const { _id, totalAmount,purchasedOn} = orderProp;
	
	return (
		<Row className="pt-3">
			<Col xs={12} md={6}>
				<Card className="courseCard p-3">
					<Card.Body>
						<Card.Title><h3>{ purchasedOn }</h3></Card.Title>			
						<Card.Subtitle><h5>totalAmount:</h5></Card.Subtitle>
						<Card.Text>&#8369; { totalAmount }</Card.Text>{/*
						<Card.Text>Enrollees: { count }</Card.Text>
						<Card.Text>Available Seats: { 30 - count }</Card.Text>*/}
						<Link className="btn btn-primary" to={`/courses/${_id}`}> View Course</Link>

					</Card.Body>

				</Card>
			</Col>
		</Row>
	)

}

OrderCards.propTypes = {
	orderProp: PropTypes.shape({
		totalAmount: PropTypes.string.isRequired,
		productOrders: PropTypes.string.isRequired
		// price: PropTypes.number.isRequired
		// photo: PropTypes.string.isRequired
	})
}


