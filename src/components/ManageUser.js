import React, { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';

import SetUserStat from './SetUserStat';
import SetUserAdmin from './SetUserAdmin';
import DeleteUser from './DeleteUser';

export default function ManageUser(props) {

	const { usersData, fetchData } = props;
	const [users, setUsers] = useState([])

	//=============Getting the coursesData from the courses page
	useEffect(() => {
		
		const usersArr = usersData.map(user1 => {			

			return (
				<tr key={user1._id}>
					<td>{user1.firstName}</td>
					<td>{user1.lastName}</td>
					<td>{user1.email}</td>
					<td>{}</td>

					<td><SetUserStat user={user1._id} isActive={user1.isActive} fetchData={fetchData}/></td>
					<td><SetUserAdmin user={user1._id} isAdmin={user1.isAdmin} fetchData={fetchData}/></td>
					<td><DeleteUser user={user1._id} fetchData={fetchData}/></td>
				</tr>

				
				)

		})
			setUsers(usersArr)


	}, [usersData, fetchData])

	return(
		<Container className="text-center">	
		   	  <Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr className="text-center">
						<th colSpan='2' className ="text-center">Name</th>
						<th>Email</th>
						<th>Address</th>
						<th colSpan='3' className="text-center">Actions</th>
						
					</tr>
				</thead>

				<tbody>
					{users}
				</tbody>
			</Table>				
		</Container>		
		)
}
