import { Button, Modal, Form } from 'react-bootstrap';
import React, { useState} from 'react';
import Swal from 'sweetalert2';
// import ImageUploading from 'react-images-uploading';

export default function AddProduct({fetchData}) {
	 // const [images, setImages] = React.useState([]);
  	 // const maxNumber = 1;
	//Add state for the forms of product
	const [productName, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [inventCount, setInventCount] = useState('')
	const [supplier, setSupplier] = useState('')
	const [prodCategory, setProdCategory] = useState('')
	const [cog, setCog] = useState('')
	let [photo, setPhoto] = useState('')
	
	// const onChangeProd = (imageList, addUpdateIndex) => {
 //    // data for submit
 //    	console.log(imageList, addUpdateIndex);
 //    	setImages(imageList);
 //    	setPhoto(images['data_url'])

 //  	};

	//States for our modals to open/close
	const [showAdd, setShowAdd] = useState(false);

	//Functions to handle opening and closing of our AddCourse Modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false)

	const addProduct = (e) => {
		e.preventDefault();

		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/products/add-product', {
			method: 'POST',
			headers: {
				"Content-Type": 'application/json',
				"Authorization": `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				productName: productName,
				description:description,
				price: price,
				inventCount: inventCount,
				supplier:supplier,
				prodCategory: prodCategory,
				cog: cog,
				photo: photo
			})
		})
		.then(res=> res.json())
		.then (data => {
			if(data){
				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: 'Product sucessfully added'
				})
				closeAdd();
				fetchData();
			} else{
				Swal.fire({
					title: "Error!",
					icon: 'error',
					text: 'Something went wrong. Please try again'
				})
				closeAdd();
				fetchData();
			}

			setName('');
			setDescription('');
			setPrice('');
			setInventCount('');
			setSupplier('');
			setProdCategory('');
			setCog('');
			setPhoto('');

		})
	}



	return(
		<>
			<Button variant="primary" onClick={openAdd}>Add New Product</Button>

		{/*Add Modal Forms*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title className ="welcome">Add Product</Modal.Title>
					</Modal.Header>

					<Modal.Body className ="p-3 m-3">
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={productName} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Inventory</Form.Label>
							<Form.Control type="number" value={inventCount} onChange={e => setInventCount(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Supplier</Form.Label>
							<Form.Control
								as="select"	 
								required
								value={supplier}
								onChange={e => setSupplier(e.target.value)}
							>		
							  <option>Select here</option>
							  <option value="Resto">Dakjuk</option>
							  <option value="Groceries">LGS</option>
							</Form.Control>
						</Form.Group>

						<Form.Group>
							<Form.Label>Product Category</Form.Label>				
							<Form.Control
								as="select"	 
								required
								value={prodCategory}
								onChange={e => setProdCategory(e.target.value)}
							>		
							  <option>Select here</option>
							  <option value="Dakjuk - Lugaw">Dakjuk - Lugaw</option>
							  <option value="Dakjuk - Mami">Dakjuk - Mami</option>
							  <option value="Dakjuk - Drinks">Dakjuk - Drinks</option>
							  <option value="Dakjuk - Short Orders">Dakjuk - Short Orders</option>			  
							</Form.Control>
						</Form.Group>

						<Form.Group>
							<Form.Label>Cost</Form.Label>
							<Form.Control type="number" value={cog} onChange={e => setCog(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Photo File Name:</Form.Label>
							<p>  (Note: It must be saved in the images folder under product. You can ask your IT admin for assistance)</p>
							<Form.Control type="text" value={photo} onChange={e => setPhoto(e.target.value)} required/>
						</Form.Group>

					
					</Modal.Body>

					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>

		</>

		)
}



