import { Button, Modal, Form } from 'react-bootstrap';
import React, { useState} from 'react';
import Swal from 'sweetalert2';

export default function EditProduct({product, fetchData}) {

	//state for courseId for the fetch URL
	const [productId, setproductId] = useState('');

	//Forms state
	//Add state for the forms of course
	const [productName, setName] = useState('');
	const [description, setDescription] = useState('')
	const [price, setPrice] = useState('')
	const [inventCount, setInventCount] = useState('')
	const [supplier, setSupplier] = useState('')
	const [prodCategory, setProdCategory] = useState('')
	const [cog, setCog] = useState('')
	const [photo, setPhoto] = useState('')

	//state for editCourse Modals to open/close
	const [showEdit, setShowEdit] = useState(false)

	//function for opening the modal
	const openEdit = (productId) => {
		//to still get the actual data from the form
		fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {
			//populate all the input values with course info that we fetched
			setproductId(data._id);
			setName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setInventCount(data.inventCount);
			setSupplier(data.supplier);
			setProdCategory(data.prodCategory);
			setCog(data.cog);
			setPhoto(data.photo);
		})

		//Then, open the modal
		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false);
		setName('')
		setDescription('')
		setPrice(0)
	}

	const editProduct = (e, productId) => {
		e.preventDefault();
		fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/products/${productId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('accessToken')}`

			},
			body: JSON.stringify({
				productName: productName,
				description:description,
				price: price,
				inventCount: inventCount,
				supplier:supplier,
				prodCategory: prodCategory,
				cog: cog,
				photo: photo

			})
		})
		.then(res => res.json())
		.then( data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Success!",
					icon: 'success',
					text: 'Product sucessfully updated'
				})
				closeEdit();
				fetchData();

			} else{
				Swal.fire({
					title: "Error!",
					icon: 'error',
					text: 'Please try again'
				})
				closeEdit();
				fetchData();
			}
		})
	}

	return(
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(product)}>Update</Button>

		{/*Edit Modal Forms*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={productName} onChange={e => setName(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Inventory</Form.Label>
							<Form.Control type="number" value={inventCount} onChange={e => setInventCount(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Supplier</Form.Label>
							<Form.Control
								as="select"	 
								required
								value={supplier}
								onChange={e => setSupplier(e.target.value)}
							>		
							  <option>Select here</option>
							  <option value="Resto">Dakjuk</option>
							  <option value="Groceries">LGS</option>
							</Form.Control>
						</Form.Group>

						<Form.Group>
							<Form.Label>Product Category</Form.Label>				
							<Form.Control
								as="select"	 
								required
								value={prodCategory}
								onChange={e => setProdCategory(e.target.value)}
							>		
							  <option>Select here</option>
							  <option value="Dakjuk - Lugaw">Dakjuk - Lugaw</option>
							  <option value="Dakjuk - Mami">Dakjuk - Mami</option>
							  <option value="Dakjuk - Drinks">Dakjuk - Drinks</option>
							  <option value="Dakjuk - Short Orders">Dakjuk - Short Orders</option>			  
							</Form.Control>
						</Form.Group>

						<Form.Group>
							<Form.Label>Cost</Form.Label>
							<Form.Control type="number" value={cog} onChange={e => setCog(e.target.value)} required/>
						</Form.Group>

						<Form.Group>
							<Form.Label>Photo</Form.Label>
							<Form.Control type="text" value={photo} onChange={e => setPhoto(e.target.value)} required/>
						</Form.Group>
					

					</Modal.Body>


					<Modal.Footer>
						<Button onClick={closeEdit}>Close</Button>
						<Button type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
				
			</Modal>
		</>
		)
}
