import React, { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';

export default function AdminViewOrderHistory(props) {


	const { ordersData, fetchData} = props;
	const [orders, setOrders] = useState([])
	 
	

	useEffect(() => {

		const orderArray = ordersData.map(order => {
		// console.log(order)
			return order.customer.map(cust => {					
				return (
					// custArr.push(cust),
					<tr key={order._id}>
						<td>{order.purchasedOn}</td>
						<td>{order.totalAmount}</td>
						<td key={cust._id}>{cust.firstName}</td>	
						<td>{cust.lastName}</td>
					</tr>	

				)
			})

		})
		setOrders(orderArray)

	}, [ordersData, fetchData])


	return(
		<Container>
			 <Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
						
					<tr className="text-center">		
						<th>Purchased On</th>
						<th>Total Amount</th>
						<th colSpan='3' className="text-center">Customer's Name</th>
						{/*<Link to={`/orders/${_id}`}><Button className="btn">View Order Details</Button> </Link>	
					<				*/}
					</tr>
				</thead>

				<tbody>
					{orders}	
				</tbody>
			</Table>		
		</Container>
		)
}
