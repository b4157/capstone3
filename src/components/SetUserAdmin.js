/*import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function SetUserAdmin({user, isAdmin, fetchData}) {

	const userAdminToggle = (userId) => {
			
			fetch(`http://localhost:4000/users/${userId}/set-admin`, {
			// fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/users/${userId}/set-admin`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					'Accept': 'application/json'
				}
			})
			.then(data => {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'user successfully reverted to normal user'
					})
					fetchData()
			})

	}

	const reactivate = (userId) => {
			fetch(`https://localhost:4000/users/${userId}/set-admin`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
			})
			.then(data => {

					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'user successfully set to admin'
					})
					fetchData()
			})
		}	
	return(
		<>
			{isAdmin ?

				<Button variant="danger" size="sm" onClick={() => userAdminToggle(user._id)}>Admin</Button>

				:

				<Button variant="success" size="sm" onClick={() => reactivate(user._id)}>Normal User</Button>
			}
		</>

		)
}*/


import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function SetUserAdmin({user, isAdmin, fetchData}) {

	const userArchiveToggle = (userId) => {
			
			fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/users/${userId}/set-admin`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					'Accept': 'application/json'
				}
			})
			.then(data => {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Course successfully disabled'
					})
					fetchData()
			})

	}

	const reactivate = (userId) => {
			fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/users/${userId}/set-admin`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
			})
			.then(data => {

					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Product successfully reactivated'
					})
					fetchData()
			})
		}	
	return(
		<>
			{isAdmin ?

				<Button variant="danger" size="sm" onClick={() => userArchiveToggle(user)}>Admin</Button>

				:

				<Button variant="success" size="sm" onClick={() => reactivate(user)}>Normal User</Button>
			}
		</>

		)
}