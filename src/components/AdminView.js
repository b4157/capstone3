import React, { useState, useEffect } from 'react';
import { Table, Container } from 'react-bootstrap';

import AddProduct from './AddProduct';
import EditProduct from './EditProduct';
import ArchiveProduct from './ArchiveProduct';
import DeleteProduct from './DeleteProduct';
import Image from 'react-bootstrap/Image';


export default function AdminView(props) {

	const { productsData, fetchData } = props;
	const [products, setProducts] = useState([])

	//=============Getting the coursesData from the courses page
	useEffect(() => {
		
		const productsArr = productsData.map(product => {
			return (
				<tr key={product._id}>
					<td>{product.productName}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.inventCount}</td>
					<td>{product.supplier}</td>
					<td>{product.prodCategory}</td>
					<td>{product.cog}</td>		
					<td><Image style={{height:'auto',width:'50%'}}  src={`../images/products/${product.photo}`} alt={product.photo}></Image></td>
					<td className={product.isActive ? "text-success" : "text-danger"}>
						{product.isActive ? "Available" : "Unavailable"}
					</td>
					<td><EditProduct product={product._id} fetchData={fetchData}/></td>
					<td><ArchiveProduct product={product._id} isActive={product.isActive} fetchData={fetchData}/></td>
					<td><DeleteProduct product={product._id} fetchData={fetchData}/></td>
				</tr>

				
				)
		})

		setProducts(productsArr)

	}, [productsData, fetchData])

	return(
		<Container className="text-center">	
		   	 <AddProduct fetchData={fetchData}/>
		   	 <Table striped bordered hover responsive>
				<thead className="bg-dark text-white">
					<tr className="text-center">
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Inventory</th>
						<th>Supplier</th>
						<th>Product Category</th>
						<th>Cost of Goods</th>
						<th>Photo</th>
						<th>Availability</th>
						<th colSpan='3' className="text-center">Actions</th>
						
					</tr>
				</thead>

				<tbody>
					{products}
				</tbody>
			</Table>				
		</Container>		
		)
}
