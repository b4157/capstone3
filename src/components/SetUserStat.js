import React from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function SetUserStat({user, isActive, fetchData}) {

	const userArchiveToggle = (userId) => {
			
			fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/users/${userId}/set-active-stat`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					'Accept': 'application/json'
				}
			})
			.then(data => {
					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Course successfully disabled'
					})
					fetchData()
			})

	}

	const reactivate = (userId) => {
			fetch(`https://laguda-grocery-store-ol-shop.herokuapp.com/users/${userId}/set-active-stat`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('accessToken')}`,
					 'Accept': 'application/json'
				}
			})
			.then(data => {

					Swal.fire({
						title: 'Success',
						icon: 'success',
						text: 'Product successfully reactivated'
					})
					fetchData()
			})
		}	
	return(
		<>
			{isActive ?

				<Button variant="danger" size="sm" onClick={() => userArchiveToggle(user)}>Archive</Button>

				:

				<Button variant="success" size="sm" onClick={() => reactivate(user)}>Unarchive</Button>
			}
		</>

		)
}