import React, { useState, useEffect } from 'react';
import {Row, Card, Button, Col } from 'react-bootstrap';
// import UserContext from '../UserContext';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function UserViewCart(props){

	const navigate = useNavigate();

	const { productsData, fetchDataProduct} = props;

	const [products, setProducts] = useState([])	

	useEffect(() => {

		const productArray = productsData.map(order => {
			
			return (
				
				order.productOrders.map( prod => {					
				return (
					// custArr.push(cust),
					<Card.Body>
						<Card.Title><h4 key={prod._id}>Product Name: {prod.productName}</h4></Card.Title>	
							<Card.Text>
								<h5 key={prod._id}>Quantity {prod.prodQty}</h5>
								<h6 key={order._id}>Sub-Total: {order.totalAmount}</h6>
							</Card.Text>	
					</Card.Body>
					)
				})
			)

		})
		
		setProducts(productArray)
		// console.log(prod)

	}, [productsData, fetchDataProduct])
	

	const checkOut = () => {

		fetch('https://laguda-grocery-store-ol-shop.herokuapp.com/orders/userCheckout', {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			},
			body: JSON.stringify({
				paymentChannel: "Cash on Delivery"
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data) {
				Swal.fire({
					title: 'Order successfully placed. Kindl wait for Deliver Confirmation',
					icon: 'success'
				})

				navigate('/menu')
			}else {
				Swal.fire({
					title:'Something went wrong',
					icon: 'error'

				})
			}
		}) 
	}

	return(
		<Row className="pt-3">
			<Col xs={12} md={6}>
				<Card className="courseCard p-3">
					{/*{orders}*/}
					{products}
				</Card>
				<Card.Footer>					
					<div className="d-grip gap-3">
						<Button variant="primary" onClick={() => checkOut()}>Check Out</Button>
					</div>
					
				</Card.Footer>
			</Col>
		</Row>
	)
}