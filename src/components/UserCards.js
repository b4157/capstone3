import React from 'react';
import	{Row, Col, Card, Button} from "react-bootstrap";
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import SpecificProduct from "../pages/SpecificProduct";

export default function UserCards({userProp}){

	const { _id, firstName, lastName, email, isAdmin, isActive, shippngInfo} = orderProp;
	
	return (
		<Row className="pt-3">
			<Col xs={12} md={6}>
				<Card className="courseCard p-3">
					<Card.Body>
						<Card.Title><h3>{ `${customerLastName}, ${customerFirstName}`}</h3></Card.Title>
						<br/>			
						<Card.Subtitle><h5>Status</h5></Card.Subtitle>
						<Card.Text className={order.isCOmplete ? "text-success" : "text-danger"}>
						{order.isCOmplete ? "Order has placed" : "Still in cart"}
						</Card.Text>
						<Link to={`/orders/${_id}`}><Button className="btn">View Order Details</Button> </Link>
					</Card.Body>

				</Card>
			</Col>
		</Row>
	)

}

ProductCards.propTypes = {
	orderProp: PropTypes.shape({
		customerLastName: PropTypes.string.isRequired,
		customerFirstName: PropTypes.string.isRequired,
		// price: PropTypes.number.isRequired
		// photo: PropTypes.string.isRequired
	})
}


